﻿using Banco.Repositorio;
using Modelo.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AlimentaBanco
{
    class Program
    {
        static void Main(string[] args)
        {
            string token, c, uid;
            List<Enterprise> empresas;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var values = new Dictionary<string, string>();
                values.Add("email", "testeapple@ioasys.com.br");
                values.Add("password", "12341234");

                var jsonString = JsonConvert.SerializeObject(values);

                var Res = client.PostAsync("http://empresas.ioasys.com.br/api/v1/users/auth/sign_in", new StringContent(jsonString, Encoding.UTF8, "application/json")).Result;

                token = Res.Headers.GetValues("access-token").FirstOrDefault();
                c = Res.Headers.GetValues("client").FirstOrDefault();
                uid = Res.Headers.GetValues("uid").FirstOrDefault();
            }

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("access-token", token);
                client.DefaultRequestHeaders.Add("client", c);
                client.DefaultRequestHeaders.Add("uid", uid);

                var Res = client.GetAsync("http://empresas.ioasys.com.br/api/v1/enterprises").Result;

                var jsonObj = Res.Content.ReadAsStringAsync().Result;

                jsonObj = jsonObj.Remove(jsonObj.Length - 1);
                jsonObj = jsonObj.Replace("{\"enterprises\":", "");

                empresas = JsonConvert.DeserializeObject<List<Enterprise>>(jsonObj);
                Console.WriteLine($"Encontrado {empresas.Count} empresas");
            }

            var tiposEmpresa = empresas.Select(x => x.EnterpriseType).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).OrderBy(x => x.Id).ToList(); // Distinct não funcionou de jeito nenhum
            Console.WriteLine($"Encontrado {tiposEmpresa.Count} tipos de empresa");


            foreach (var item in tiposEmpresa)
            {
                using (var rep = new Repositorio<EnterpriseType>(new Banco.Contexto.Contexto()))
                {
                    if (rep.ObtemPorId(item.Id) == null)
                    {
                        rep.Adicionar(new EnterpriseType() { EnterpriseTypeName = item.EnterpriseTypeName }).Wait();
                        Console.WriteLine($"Inserido Enterprisetype {item.EnterpriseTypeName}");
                    }
                }
            }

            foreach (var item in empresas)
            {
                using (var rep = new RepositorioEnterprise(new Banco.Contexto.Contexto()))
                {
                    if (rep.Buscar(x=>x.Name.Equals(item.Name)).FirstOrDefault() == null)
                    {
                        Enterprise e = new Enterprise()
                        {
                            City = item.City,
                            Country = item.Country,
                            Description = item.Description,
                            EmailEnterprise = item.EmailEnterprise,
                            Facebook = item.Facebook,
                            Linkedin = item.Linkedin,
                            Name = item.Name,
                            OwnEnterprise = item.OwnEnterprise,
                            OwnShares = item.OwnShares,
                            Phone = item.Phone,
                            Photo = item.Photo,
                            SharePrice = item.SharePrice,
                            Shares = item.Shares,
                            Twitter = item.Twitter,
                            Value = item.Value
                        };
                        rep.AdicionarEnterpriseType(e, item.EnterpriseType.Id);

                        rep.Adicionar(e).Wait();
                        Console.WriteLine($"Inserido Enterprise {item.Name}");
                    }
                }
            }
        }
    }
}

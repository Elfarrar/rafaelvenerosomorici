﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco.Contexto
{
    public class Contexto : DbContext
    {
        public Contexto() : base("name=IoasysEmpresa")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Login> Login { get; set; }
        public DbSet<Investor> Investor { get; set; }
        public DbSet<Portfolio> Portfolio { get; set; }
        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties().Where(x => x.Name == "Id").Configure(x => x.IsKey());
            modelBuilder.Properties().Where(x => x.PropertyType == typeof(string)).Configure(x => x.HasMaxLength(100));

            modelBuilder.Entity<Enterprise>().Property(x => x.Description).IsMaxLength();

            modelBuilder.Entity<Enterprise>().HasRequired(x => x.EnterpriseType).WithMany();
            modelBuilder.Entity<Investor>().HasOptional(x => x.Portfolio);
            modelBuilder.Entity<Portfolio>().HasMany(x => x.Enterprises);

            base.OnModelCreating(modelBuilder);
        }
    }
}

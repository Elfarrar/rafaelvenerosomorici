namespace Banco.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Banco.Contexto.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Banco.Contexto.Contexto context)
        {
            context.Login.AddOrUpdate(new Modelo.Entidades.Login() { email = "testeapple@ioasys.com.br", password = "12341234", permissao = Modelo.Entidades.Enum.Permissao.Admin });

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}

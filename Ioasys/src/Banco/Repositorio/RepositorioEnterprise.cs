﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco.Repositorio
{
    public class RepositorioEnterprise : Repositorio<Enterprise>
    {
        public RepositorioEnterprise(Contexto.Contexto db) : base(db)
        {

        }

        public void AdicionarEnterpriseType(Enterprise obj, int id)
        {
            obj.EnterpriseType = contexto.EnterpriseType.Find(id);
        }
    }
}

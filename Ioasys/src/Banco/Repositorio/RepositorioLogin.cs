﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco.Repositorio
{
    public class RepositorioLogin:Repositorio<Login>
    {
        public RepositorioLogin(Contexto.Contexto db):base(db)
        {
            
        }

        public Login Logar(string email, string senha)
        {
            return Buscar(x => x.email.Equals(email) && senha.Equals(senha)).FirstOrDefault();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Banco.Repositorio
{
    public class Repositorio<T> : IDisposable, IRepositorio<T> where T : class
    {
        protected readonly Contexto.Contexto contexto;
        public Repositorio(Contexto.Contexto contexto)
        {
            this.contexto = contexto;
        }

        public void Dispose()
        {
            contexto.Dispose();
        }

        public IQueryable<T> Buscar(Expression<Func<T, bool>> predicate)
        {
            return Tudo().Where(predicate);
        }

        public T ObtemPorId(int id)
        {
            return contexto.Set<T>().Find(id);
        }

        public IQueryable<T> Tudo()
        {
            return contexto.Set<T>();
        }

        public IQueryable<T> Buscar(IQueryable<T> query, Expression<Func<T, bool>> predicate)
        {
            return query.Where(predicate);
        }

        public async Task Adicionar(T obj)
        {
            contexto.Set<T>().Add(obj);
            await contexto.SaveChangesAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Banco.Repositorio
{
    public interface IRepositorio<T> where T : class
    {
        T ObtemPorId(int id);
        Task Adicionar(T obj);
        IQueryable<T> Buscar(IQueryable<T> query, Expression<Func<T, bool>> predicate);
        IQueryable<T> Buscar(Expression<Func<T, bool>> predicate);
        IQueryable<T> Tudo();
    }
}

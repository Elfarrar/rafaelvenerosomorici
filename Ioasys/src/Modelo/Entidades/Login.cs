﻿using Modelo.Entidades.Enum;

namespace Modelo.Entidades
{
    public class Login:Base
    {
        public int Id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public Permissao permissao { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public class Enterprise:Base
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("enterprise_name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("email_enterprise")]
        public string EmailEnterprise { get; set; }

        [JsonProperty("facebook")]
        public string Facebook { get; set; }

        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        [JsonProperty("linkedin")]
        public string Linkedin { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("own_enterprise")]
        public bool OwnEnterprise { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("shares")]
        public int? Shares { get; set; }

        [JsonProperty("share_price")]
        public decimal SharePrice { get; set; }

        [JsonProperty("own_shares")]
        public int? OwnShares { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("enterprise_type")]
        public EnterpriseType EnterpriseType { get; set; }
    }
}

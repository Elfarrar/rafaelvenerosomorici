﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public class Base:IDisposable
    {
        public void Dispose()
        {
            this.Dispose();
        }
    }
}

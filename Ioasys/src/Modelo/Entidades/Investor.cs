﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Investor:Base
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("investor_name")]
        public string InvestorName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("balance")]
        public int Balance { get; set; }

        [JsonProperty("photo")]
        public object Photo { get; set; }

        [JsonProperty("portfolio")]
        public Portfolio Portfolio { get; set; }

        [JsonProperty("portfolio_value")]
        public int PortfolioValue { get; set; }

        [JsonProperty("first_access")]
        public bool FirstAccess { get; set; }

        [JsonProperty("super_angel")]
        public bool SuperAngel { get; set; }
    }
}

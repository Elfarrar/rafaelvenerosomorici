﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public class EnterpriseType:Base
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("enterprise_type_name")]
        public string EnterpriseTypeName { get; set; }
    }
}

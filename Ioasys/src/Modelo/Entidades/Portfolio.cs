﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Portfolio:Base
    {
        public Portfolio()
        {
            Enterprises = new List<Enterprise>();
        }
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("enterprises_number")]
        public int EnterprisesNumber {
            get
            {
                return Enterprises.Count;
            }
        }

        [JsonProperty("enterprises")]
        public virtual List<Enterprise> Enterprises { get; set; }
    }
}

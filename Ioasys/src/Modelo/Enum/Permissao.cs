﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades.Enum
{
    public enum Permissao
    {
        Admin = 0,
        Supervisor = 1,
        Usuario = 2
    }
}

﻿using Banco.Contexto;
using Banco.Repositorio;
using Microsoft.Owin.Security.OAuth;
using Modelo.Entidades;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API
{
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                string email = context.UserName;
                string senha = context.Password;

                Login user;

                using (var bd = new RepositorioLogin(new Contexto()))
                {
                    user = bd.Logar(email, senha);
                }

                if (user == null)
                {
                    context.SetError("invalid_grant", "E-mail ou senha errados");
                    return;
                }

                List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name , user.email),
                    new Claim("UserID", user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.permissao.ToString())
                };

                ClaimsIdentity OAuthIdentity = new ClaimsIdentity(claims, Startup.OAuthOptions.AuthenticationType);
                context.Validated(new Microsoft.Owin.Security.AuthenticationTicket(OAuthIdentity, new Microsoft.Owin.Security.AuthenticationProperties() { }));
            });
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
                context.Validated();

            return Task.FromResult<object>(null);
        }
    }
}
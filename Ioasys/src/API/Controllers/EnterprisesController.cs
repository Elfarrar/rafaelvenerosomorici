﻿using Banco.Repositorio;
using Modelo.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [Authorize]
    public class EnterprisesController : ApiController
    {
        // GET: api/Enterprises
        public string Get()
        {
            List<Enterprise> empresas;

            var param = Request.GetQueryNameValuePairs();

            using (var rep = new RepositorioEnterprise(new Banco.Contexto.Contexto()))
            {
                var enterprises = rep.Tudo();

                foreach (var item in param)
                {
                    if (item.Key.Equals("name"))
                        enterprises = rep.Buscar(enterprises, x => x.Name.Contains(item.Value));
                    else
                    {
                        int id = Convert.ToInt32(item.Value);
                        enterprises = rep.Buscar(enterprises, x => x.EnterpriseType.Id == id);
                    }
                }

                empresas = enterprises.ToList();
            }
            return JsonConvert.SerializeObject(empresas);
        }

        // GET: api/Enterprises/5
        public string Get(int id)
        {
            Enterprise empresa;
            using (var rep = new RepositorioEnterprise(new Banco.Contexto.Contexto()))
            {
                empresa = rep.ObtemPorId(id);
            }
            return JsonConvert.SerializeObject(empresa);
        }
    }
}
